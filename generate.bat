@echo off
call setup.bat
cd src
doxygen ../config.ini functions.h
cd ..
java -cp %SAXONDIR%/saxon9he.jar net.sf.saxon.Transform -s:src/doxygen/xml/functions_8h.xml -xsl:functions.xsl -o:output
for %%a in (functions/shapes/functions/*.shape) do magick convert -size 20x20 xc:white -font "Arial" -pointsize 8 -fill black -draw "text 0,12 '%%~na'" functions/shapes/functions/%%~na.png
rmdir /s /q %DIADIR%\shapes\functions
xcopy /s /y functions %DIADIR%
rmdir /s /q src\doxygen
rmdir /s /q functions