@echo off
call setup.bat
IF EXIST %1 del /F %1
IF EXIST %1.xml del /F %1.xml
7z x %1.dia
ren %1 %1.xml
java -cp %SAXONDIR%/saxon9he.jar net.sf.saxon.Transform -s:%1.xml -xsl:compile.xsl -o:src/program.c
IF EXIST %1 del /F %1
IF EXIST %1.xml del /F %1.xml
cd src
make
cls
program
cd ..