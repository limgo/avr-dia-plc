<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dia="http://www.lysator.liu.se/~alla/dia/" >
<xsl:output method="text" indent="no" omit-xml-declaration="yes"/>
<xsl:template match="text()"/>
<xsl:template match="/" >
<xsl:text>#include "program.h" 
</xsl:text>
<xsl:text>void loop(){</xsl:text>
<xsl:apply-templates select="dia:diagram/dia:layer/dia:object"/>
<xsl:text>}</xsl:text>
</xsl:template>
	<xsl:template match="dia:diagram/dia:layer/dia:object" name="loop">
		<xsl:param name="function" select="."/>			
		<xsl:param name="print" select="'x'"/>
		<xsl:param name="argumentId" select="0"/>
		<xsl:param name="numberOfArgs" select="0"/>
		
		<xsl:variable name="id" select="$function/@id"/>
		<xsl:variable name="name" select="$function/@type"/>	
	
		<!--<xsl:for-each select="dia:object">-->
			<xsl:if test="not(dia:connections)">
				<!-- Check if function is passed to another function as parameter -->
				<xsl:variable name="refs">									
					<xsl:choose>
						<xsl:when test="$print='x'">
							<xsl:for-each select="../dia:object/dia:connections">					
								<xsl:if test="$id=dia:connection[1]/@to">
									<xsl:value-of select="1" />
								</xsl:if> 										
							</xsl:for-each>					
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$print" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="colon">
					<xsl:if test="$refs='' and $print='x'">
						<xsl:text>;</xsl:text>
					</xsl:if>
				</xsl:variable>
				
				<xsl:if test="$refs='' or $print=''">
					<xsl:value-of select="$name"/><xsl:text>(</xsl:text>
					<!-- VALUE <xsl:value-of select="@value"/> -->
				</xsl:if >
				<!-- Check num of args by checking how many inputs are referenced (output is if handle=0) -->
				<xsl:variable name="argCount" select="count(//dia:layer/dia:object/dia:connections/dia:connection[@to=$id and @handle!=0])"/>
				<xsl:for-each select="//dia:layer/dia:object/dia:connections">															
					<xsl:if test="$id=dia:connection[2]/@to">												
						<xsl:variable name="sourceId" select="dia:connection[1]/@to"/>
						<xsl:call-template name="loop">
							<xsl:with-param name="function" select="../../dia:object[@id=$sourceId]"/>
							<xsl:with-param name="print" select="$refs"/>
							<xsl:with-param name="argumentId" select="dia:connection[2]/@connection"/>
							<xsl:with-param name="numberOfArgs" select="$argCount"/>
						</xsl:call-template>
					</xsl:if >					
				</xsl:for-each>
				<xsl:if test="$refs='' or $print=''">
					<xsl:if test="$argCount=0">
						<xsl:value-of select="translate($function/dia:attribute[@name='custom:Value']/dia:string, '#', '')" />
					</xsl:if>
					<xsl:text>)</xsl:text>
					<xsl:if test="$argumentId+1 lt $numberOfArgs"><xsl:text>,</xsl:text></xsl:if>
					<xsl:value-of select="$colon"/>
				</xsl:if>
					
			</xsl:if> 
	</xsl:template>

</xsl:stylesheet>