<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" omit-xml-declaration="yes"/>
  
	<xsl:template match="doxygen">
	
		<xsl:for-each select="compounddef/sectiondef/memberdef[@kind='function']">

			<xsl:result-document method="xml" href="functions/shapes/functions/{name}.shape">
				
<xsl:variable name="paramCount" select="count(param)"/>				
<xsl:variable name="numOfParams">
	<xsl:choose>
		<xsl:when test="$paramCount=0">								
			<xsl:value-of select="1"/>				
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$paramCount" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

				
<shape xmlns="http://www.daa.com.au/~james/dia-shape-ns" xmlns:svg="http://www.w3.org/2000/svg">
<name><xsl:value-of select="name"/></name>
<icon><xsl:value-of select="name"/>.png</icon>
<description><xsl:value-of select="name"/></description>
<connections>



<xsl:for-each select="param">
<xsl:variable name="yPos" select="position()"/>
<point x="0" y="{$yPos*20+5}"/>
</xsl:for-each>
<point x="50" y="{($numOfParams*20+30) div 2}"/>

</connections>

<ext_attributes>
<ext_attribute name="Value" type="string" />
</ext_attributes>

<aspectratio type="free"/>
<svg:svg width="3.0" height="3.0">



<svg:rect style="fill: #888888" x="0" y="0" width="50" height="{$numOfParams*20+20}"/>

<svg:text style="font-size: 12px; text-anchor: middle" x="25" y="10" ><xsl:value-of select="name"/></svg:text>
<xsl:for-each select="param">

<xsl:variable name="yPos" select="position()*20+5"/>
<svg:polygon points="5,{$yPos} 0,{$yPos - 5} 0,{$yPos+5}" style="fill: #444444"/>
<svg:text style="font-size: 12px; text-anchor: start" x="8" y="{$yPos + 1}" ><xsl:value-of select="declname"/>:<xsl:value-of select="type"/></svg:text>
</xsl:for-each>

<xsl:variable name="yMiddle" select="($numOfParams*20+30) div 2"/>
<svg:polygon points="45,{$yMiddle} 50,{$yMiddle - 5} 50,{$yMiddle+5}" style="fill: #444444"/>
<svg:text style="font-size: 12px; text-anchor: end" x="43" y="{$yMiddle + 1}" >out:<xsl:value-of select="type"/></svg:text>

</svg:svg>
</shape>
	
			</xsl:result-document>

		</xsl:for-each>
		
		<xsl:result-document method="xml" href="functions/sheets/functions.sheet">
			<sheet xmlns="http://www.lysator.liu.se/~alla/dia/dia-sheet-ns">
			<name>Functions</name>
			<description>Functions</description>
			<contents>
			<xsl:for-each select="compounddef/sectiondef/memberdef[@kind='function']">
				<xsl:variable name="name" select="name"/>
				<object name="{$name}"><description name="{$name}"></description></object>
			</xsl:for-each>
			</contents>
			</sheet>
		</xsl:result-document>
		
	</xsl:template>
  
</xsl:stylesheet>